<?php

/**
 * Application routes.
 */
Route::get('/', 'PageController@home')->name('home');
Route::get('politicas', 'PageController@politicas')->name('politicas');
Route::get('contato', 'PageController@contato')->name('contato');
Route::post('contato', 'PageController@contato')->name('contato');
Route::get('empreendimento/{slug}', 'PageController@empreendimento')->name('empreendimento');
Route::get('empreendimentos', 'PageController@empreendimentos')->name('empreendimentos');
Route::get('noticias', 'PageController@noticias')->name('noticias');
Route::get('quem-somos', 'PageController@quemSomos')->name('quem-somos');
Route::get('venda-seu-imovel', 'PageController@venda')->name('venda');
Route::get('search/{query?}', 'PageController@search')->name('search');
Route::get('noticia/{slug}', 'PageController@noticia')->name('noticia');
