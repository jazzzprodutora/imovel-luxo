@extends('layouts.app')

@section('content')
	<div class="container">
		<h2>Página não encontrada</h2>
		<p>
			Desculpe, a página que você procura não existe. Por favor, utilziar o menu acima.
		</p>
	</div>
	<footer>
@endsection
