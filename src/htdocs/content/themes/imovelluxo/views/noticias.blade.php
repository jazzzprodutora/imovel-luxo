@extends('layouts.app')

@section('content')
    <main class="noticias-page">
        <section class="titulo-interno">
            <div class="container">
                <h1>Notícias Imobiliária</h1>
            </div>
        </section>
        <div class="container">
            <div class="breadcrumbs">
                @include('partials.breadcrumbs')
            </div>
        <section class="noticias-page-cont container">
            @foreach($result as $key => $rs)
                @php
                    $thumbnail_medium = $rs->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_MEDIUM);
                    $thumbnail_large = $rs->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_LARGE);
                    $thumbnail_full = $rs->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_FULL);
                    if(is_array($thumbnail_full)){
                        $thumbnail_full = $thumbnail_full['url'];
                    }
                    if(is_array($thumbnail_large)){
                        $thumbnail_large = $thumbnail_large['url'];
                    }
                    \Jenssegers\Date\Date::setLocale('pt_BR');
                @endphp
                <a href="{{route('noticia', $rs->post_name)}}">
                    <article class="noticia-item">
                        <picture class="img-noticia">
                            <img width="{{$thumbnail_medium['width']}}" height="{{$thumbnail_medium['height']}}" src="{{$thumbnail_full}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="{{$thumbnail_large}} 344w, {{$thumbnail_medium['url']}} 300w" sizes="(max-width: 344px) 100vw, 344px" />
                        </picture>
                        <div class="cont-noticias-page">
                            <img class="icon-contato" src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/calendario.svg"><span class="data-noticia">
                                {{\Jenssegers\Date\Date::createFromTimeString($rs->post_date)->format('d')}} de
                                {{\Jenssegers\Date\Date::createFromTimeString($rs->post_date)->format('F')}} de
                                {{\Jenssegers\Date\Date::createFromTimeString($rs->post_date)->format('Y')}}
                            </span>
                            <h3>{{$rs->post_title}}</h3>
                            <p><p>{!!$rs->post_content!!}</p>
                            </p>
                            <h5>Saiba mais</h5>
                        </div>
                    </article>
                </a>
            @endforeach
        </section>
        </div>
    </main>
@endsection
