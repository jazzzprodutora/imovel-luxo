@extends('layouts.app')

@section('content')
<main class="noticia-destaque">
            <section class="titulo-interno">
                <div class="container">
                    <h1>Notícias</h1>
                </div>
            </section>
            <div class="container">
                @include('partials.breadcrumbs')
            </div>
            <section class="content-noticia-destaque container">
                @php
                    $thumbnail_medium = $result->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_MEDIUM);
                    $thumbnail_large = $result->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_LARGE);
                    $thumbnail_full = $result->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_FULL);
                    if(is_array($thumbnail_full)){
                        $thumbnail_full = $thumbnail_full['url'];
                    }
                    if(is_array($thumbnail_large)){
                        $thumbnail_large = $thumbnail_large['url'];
                    }
                    \Jenssegers\Date\Date::setLocale('pt_BR');
                @endphp
                    <article class="item-noticia">
                        <div class="titulos-noticia">
                            <h1>{{$result->post_title}}</h1>
                            <img class="icon-contato" src="{{asset('content/themes/imovelluxo/assets/icons/calendario.svg')}}"><span class="data-noticia">{{\Jenssegers\Date\Date::createFromTimeString($result->post_date)->format('d/m/Y')}}</span>
                        </div>
                        <picture class="img-destaque">
                        <img src="{{$thumbnail_full}}" alt="">
                        </picture>
                        <div class="item-content">
                            <p>{!!$result->post_content!!}</p>
                        </div>
                        @if(!empty($result->__get('videos')))
                            <section class="video-imovel">
                                <!-- <video width="100%" autoplay loop muted>
                                    <source src="" type="video/mp4">
                                    <source src="movie.ogg" type="video/ogg">
                                    Seu navegador não suporta este vídeo
                                </video> -->
                                {!! $video !!}
                            </section>
                        @endif
                        <div class="sharify-container"><ul><li class="sharify-btn-twitter">
                                    <a title="Tweet on Twitter" href="https://twitter.com/intent/tweet?text={{$result->post_title}}: {{url()->current()}}" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                        <span class="sharify-icon"><i class="sharify sharify-twitter"></i></span>
                                        <span class="sharify-title">Tweet</span>
                                    </a>
                                </li><li class="sharify-btn-facebook">
                                    <a title="Share on Facebook" href="http://www.facebook.com/sharer.php?u={{urlencode(url()->current())}}" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                        <span class="sharify-icon"><i class="sharify sharify-facebook"></i></span>
                                        <span class="sharify-title">Share</span>
                                        <span class="sharify-count">0</span>
                                    </a>
                                </li><li class="sharify-btn-linkedin">
                                    <a title="Share on Linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title={{$result->post_title}}" onclick="if(!document.getElementById('td_social_networks_buttons')){window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;}" >
                                        <span class="sharify-icon"><i class="sharify sharify-linkedin"></i></span>
                                        <span class="sharify-title">LinkedIn</span>
                                        <span class="sharify-count">0</span>
                                    </a>
                                </li><li class="sharify-btn-email">
                                    <a title="Share via mail" href="mailto:?subject={{$result->post_title}}&body={{$options_email_msg}} - {{url()->current()}}">
                                        <span class="sharify-icon"><i class="sharify sharify-mail"></i></span>
                                        <span class="sharify-title">Email</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="fb-comments" data-href="https://developers.facebook.com/docs/plugins/comments#configurator" data-numposts="5" data-width=""></div>
                    </article>
                    <!-- <?php //endwhile;?> <?php //endif; ?> -->
                 <!-- Outras notícias -->
                    <aside class="outras-noticias">
                        <h3>Notícias recentes</h3>
                        <ul class="list-unstyled">
                            @foreach($noticias as $noticia)
                                <a href="{{route('noticia', $noticia->post_name)}}">
                                    <div class="texto-novi">
                                        <h4>{{$noticia->post_title}}</h4>
                                    </div>
                                </a>
                            @endforeach
                        </ul>
                    </aside>
                </section>
            </main>
  @endsection
