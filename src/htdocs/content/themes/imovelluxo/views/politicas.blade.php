@extends('layouts.app')

@section('content')
        <main class="politicas">
            <section class="titulo-interno">
                <div class="container">
                    <h1>Nossas Políticas</h1>
                </div>
            </section>
            <div class="container">
                <!-- Plugin de breadcrumbs <?php // get_template_part( 'templates/breadcrumbs' ); ?> -->
            </div>
            <section class="container">
                <div class="chamada-politicas">
                    <h2>Informe-se a respeito das nossas políticas de funcionamento</h2>
                </div>
                <div class="row cont-politicas">
                    <nav class="col-lg-4 col-xl-3">
                        <ul class="list-unstyled">
                            <li><a href="#privacidade">POLÍTICA DE PRIVACIDADE</a></li>
                            <li><a href="#qualidade">POLÍTICA DE QUALIDADE</a></li>
                            <li><a href="#seguranca">DICAS DE SEGURANÇA</a></li>
                            <li><a href="#termos">TERMOS DE USO</a></li>
                        </ul>
                    </nav>
                    <div class="conteudo-politicas col-lg-8 col-xl-9">
                        <article id="privacidade">
                            <h3>Políticas de privacidade</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut qui quod molestias perspiciatis cumque corporis quia alias atque velit. Modi, dicta. Praesentium fuga, quas rem ipsam tempoloremre Lorem, ipsum dolor sit amet consectetur adipisicing elit. Ducimus facilis adipisci, cumque atque assumenda corrupti quas animi minima aperiam. Ducimus possimus, necessitatibus est consequuntur repellendus vel aliquid commodi quisquam ipsum! Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugiat quisquam exercitationem non cum quod deserunt autem, repudiandae ut quidem nulla eligendi tempora, neque laboriosam, nobis veritatis ipsam deleniti laborum saepe? ratione est nam!</p>
                        </article>
                        <article id="qualidade">
                            <h3>Políticas de qualidade</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur Lorem ipsum dolor sit amet, consectetur adipisicing elit. Officiis eos officia facere fugit eveniet magnam ipsam voluptate quos itaque accusantium non est delectus ratione, doloremque, corporis eaque voluptates maiores molestiae. adipisicing elit. Ut qui quod molestias perspiciatis cumque corporis quia alias atque velit. Modi, dicta. Praesentium fuga, quas rem ipsam tempore ratione est nam!</p>
                        </article>
                        <article id="seguranca">
                            <h3>Dicas de segurança</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat sint beatae deserunt non recusandae facere maxime nostrum animi. Vel provident accusantium quas et, amet maxime explicabo tenetur aut voluptas quo! elit. Ut qui quod molestias perspiciatis cumque corporis quia alias atque velit. Modi, dicta. Praesentium fuga, quas rem ipsam tempore ratione est nam!</p>
                        </article>
                        <article id="termos">
                            <h3>Termos de uso</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut qui quod molestias perspiciatis cumque corporis quia alias atque velit. Modi, dicta. Praesentium fuga, quas rem ipsam tempore ratione est nam Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse iure corrupti earum quibusdam similique consequatur totam quidem ullam dicta molestias ipsa beatae veritatis ratione, facilis culpa quae at sequi provident? Lorem ipsum, dolor sit amet consectetur adipisicing elit. A id ut fugiat, magni aliquam modi assumenda ipsum voluptatum nesciunt odit. Deleniti rerum ullam voluptas magni veritatis repellat facilis quod iusto? Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique libero rem, laudantium qui ducimus sed esse explicabo praesentium unde voluptas necessitatibus odit magni facilis eos veritatis quae adipisci deleniti impedit! !</p>
                        </article>
                    </div>
                </div>
            </section>
        </main>
        @endsection
