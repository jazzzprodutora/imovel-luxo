@extends('layouts.app')

@section('content')
        <main class="empreendimento">
            <section class="titulo-interno">
                <div class="container">
                    <h1>Todos os Imóveis</h1>
                </div>
            </section>
            <div class="container">
                <!-- <?php //get_template_part( 'templates/breadcrumbs' ); ?> -->
            </div>
            <section class="content-empreendimento container">
                <p>Foram encontrado(s) Imóveis(s) para a busca:</p>
                <div class="row">
                @foreach($result as $rs)
                    @include('partials.empreendimento')
                    @endforeach
                </div>
            </section>
        </main>
        @endsection
