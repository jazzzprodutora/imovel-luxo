@extends('layouts.app')

@section('content')
    <main class="contato">
        <section class="titulo-interno">
            <div class="container">
                <h1>Contato</h1>
            </div>
        </section>
        <div class="container">
            @include('partials.breadcrumbs')
        </div>
        <section class="contato-conteudo container">
            <div class="onde-estamos">
                <h2>Onde estamos</h2>
                <ul class="list-unstyled">
                    <li><a href=""><img class="icon-contato" src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/contato/whatsapp.svg" alt="whatsapp">(81) <strong>9 9264-4424</strong></a></li>
                    <li><a href=""><img class="icon-contato" src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/contato/tel.svg" alt="Telefone">(81) <strong>3040-3200</strong></a></li>
                    <li><a class="email-destaque" href="mailto:contato@imovelluxo.com.br"><img class="icon-contato" src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/contato/email.svg" alt="Email"> contato@imovelluxo.com.br</a></li>
                    <li><address><a href=""><img class="icon-contato" src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/contato/pin.svg" alt="Endereço"><p>Rua Antônio Lumack, 128<br> Boa Viagem, Recife/PE <br>CEP: 51020-350 </p></a> </address></li>
                </ul>
            </div>
            <div class="form-contato">
                <h2>Fale Conosco</h2>
                <?php echo do_shortcode( '[contact-form-7 id="36" title="Contato"]' ); ?>
        </section>
        <aside class="container localizacao">
            <h2>Localização</h2>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3949.8373398208146!2d-34.90412228566987!3d-8.118038783485863!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7ab1fadc47af69d%3A0x22e38decd333b5ca!2sIM%C3%93VELLUXO!5e0!3m2!1spt-BR!2sbr!4v1589207395768!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </aside>
    </main>
@endsection
