@extends('layouts.app')

@section('content')
    <main class="quem-somos">
        <section class="titulo-interno">
            <div class="container">
                <h1>Quem Somos</h1>
            </div>
        </section>
        <!-- Migalha de pão -->
        <div class="container">
            <div class="breadcrumbs">
                @include('partials.breadcrumbs')
            </div>
        <!-- Sobre -->
        <section class="quemsomos-conteudo container">
            <div class="conteudo-quemsomos">
                <h2>Olá, somos a ImóvelLuxo</h2>
                <p>Seja bem-vindo à ImóvelLuxo, a especialista em avaliação, venda, locação e consultoria em imóveis de alto padrão, residenciais, comerciais e rurais.
                    Atuamos em Recife, litoral e outras cidade do Brasil. Já estamos há mais de 20 anos no mercado.</p>
                <p>Construindo relacionamento de confiança com nossos clientes, repassando as informações, diretas, objetivas, transparentes e seguras. Buscamos a excelência sempre pensando no seu bem-estar. Deixando-o totalmente à vontade e seguro na hora de concluir o negócio.</p>
            </div>
            <picture class="img-quemsomos">
                <source media="(max-width:460px)" srcset="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/imgs/imovel-quem-somos-sm.jpg" >
                <img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/imgs/imovel-quem-somos.jpg" alt="">
            </picture>
        </section>
        <!-- Sobre o corretor -->
        <aside class="corretor container">
            <h2>Consulte Nosso Especialistas em ImovelLuxo.</h2>
            <div class="owl-carousel owl-theme corretor-cont">
                @if(!empty($array_equipes))
                <div class="item">
                    @foreach($array_equipes as $equipe)
                        <article class="corretor-item">
                            @if(isset($equipe['imagem']))
                                <picture class="img-corretor">
                                    <img src="http://imovelluxo.com.br/wp-content/uploads/{{$equipe['imagem']}}" alt="">
                                </picture>
                            @endif
                            <div class="info-corretor">
                                @if(isset($equipe['nome']))
                                    <h3>{{$equipe['nome']}}</h3>
                                @endif
                                @if(isset($equipe['funcao']))
                                    <span>{{$equipe['funcao']}}</span>
                                @endif
                                @if(isset($equipe['telefone']))
                                    <a href="">{{$equipe['telefone']}}</a>
                                @endif
                                @if(isset($equipe['email']))
                                    <a href="">{{$equipe['email']}}</a>
                                @endif
                            </div>
                        </article>
                    @endforeach
                </div>
                @endif
            </div>
        </aside>
    </main>

    <script>
        $('.corretor-cont').owlCarousel({
            loop:true,
            autoplay:true,
            margin:10,
            responsiveClass:true,
            dots:false,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:1,
                    nav:false
                },
                1000:{
                    items:1,
                    nav:true,
                }
            }
        })
    </script>
@endsection
