@extends('layouts.app')

@section('content')
            <main class="empreendimentos">
                <section class="titulo-interno">
                    <div class="container">
                        <h1>Imóveis</h1>
                    </div>
                </section>

                <div class="container">
                    <!-- <?php //get_template_part( 'templates/breadcrumbs' ); ?> -->
                </div>
                <div class="container">
                    <article class="content-empreendimento">
                        <section class="topo-empreendimento">
                            @php
                                $thumbnail_full = $result->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_FULL);
                                if(is_array($thumbnail_full)){
                                    $thumbnail_full = $thumbnail_full['url'];
                                }

                            @endphp
                            <div class="img-destaque">
                                <img src="{{asset($thumbnail_full)}}" alt="">
                            </div>
                            <div class="outras-galeria">
                                <div class="owl-carousel owl-theme fotos-destaque">
                                    @if(!empty($result->__get('galeria')))
                                        @foreach($post_gallery as $gallery)
                                            <div class="item"  >
                                                @foreach($gallery as $imgs)
                                                    <img src="http://imovelluxo.com.br/wp-content/uploads/{{$imgs}}" data-toggle="modal" data-target="#id-img">
                                                @endforeach
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </section>
                        <section class="info-empreendimentos"  id="interesse">
                            <h2 class="titulo-empreendimento">{{$result->post_title}}</h2>
                            <address>
                                <img class="icon-contato" src="{{asset('content/themes/imovelluxo/assets/icons/contato/pin.svg')}}" alt="Endereço"> <span>Endereço</span>
                            </address>
                            <div class="info" >
                                <div class="cont-info">
                                    <div class="sobre-imovel">
                                        <div class="cont-sobre">
                                            <h3>Sobre o imóvel</h3>
                                        <p>{{$result->__get('sobre-imovel')}}</p>
                                        </div>
                                        <div class="detalhes">
                                            <h3>Detalhes do imóvel</h3>
                                            <ul class="list-unstyled">
                                                <!-- Os ícones de metros, quartos, suítes e vagas serão fixo, não precisará de cadastro, apenas o conteúdo -->
                                                <li><img src="{{asset('content/themes/imovelluxo/assets/icons/empreendimentos/metros.jpg')}}" alt="1 m²"><span>{{$result->__get('info_metros')}}</span></li>
                                                <li><img src="{{asset('content/themes/imovelluxo/assets/icons/empreendimentos/quartos.jpg')}}" alt="2 quartos"><span>{{$result->__get('info_quartos')}}</span></li>
                                                <li><img src="{{asset('content/themes/imovelluxo/assets/icons/empreendimentos/suites.png')}}" alt="2 suítes"><span>{{$result->__get('info_suites')}}</span></li>
                                                <li><img src="{{asset('content/themes/imovelluxo/assets/icons/empreendimentos/vagas.jpg')}}" alt="2 vagas"><span>{{$result->__get('info_vagas')}}</span></li>
                                                <!-- Este loop poderá dar mais opções para cadastro -->
                                                @if(!empty($detalhes_imovel))
                                                    @foreach($detalhes_imovel as $detalhe_imovel)
                                                        <li><img src="http://imovelluxo.com.br/wp-content/uploads/{{$detalhe_imovel['icon']}}" alt="info"><span>{{$detalhe_imovel['info']}}</span></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="condominio">
                                            <h3>Sobre o condomínio</h3>
                                        <p>{{$result->__get('sobre-condominio')}}</p>
                                        </div>

                                    </div>
                                    <div class="compartilhamento">
                                        <div class="sharify-container"><ul><li class="sharify-btn-twitter">
                                                    <a title="Tweet on Twitter" href="https://twitter.com/intent/tweet?text={{$result->post_title}}: {{url()->current()}}" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                        <span class="sharify-icon"><i class="sharify sharify-twitter"></i></span>
                                                        <span class="sharify-title">Tweet</span>
                                                    </a>
                                                </li><li class="sharify-btn-facebook">
                                                    <a title="Share on Facebook" href="http://www.facebook.com/sharer.php?u={{urlencode(url()->current())}}" onclick="window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;">
                                                        <span class="sharify-icon"><i class="sharify sharify-facebook"></i></span>
                                                        <span class="sharify-title">Share</span>
                                                        <span class="sharify-count">0</span>
                                                    </a>
                                                </li><li class="sharify-btn-linkedin">
                                                    <a title="Share on Linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}&title={{$result->post_title}}" onclick="if(!document.getElementById('td_social_networks_buttons')){window.open(this.href, 'mywin','left=50,top=50,width=600,height=350,toolbar=0'); return false;}" >
                                                        <span class="sharify-icon"><i class="sharify sharify-linkedin"></i></span>
                                                        <span class="sharify-title">LinkedIn</span>
                                                        <span class="sharify-count">0</span>
                                                    </a>
                                                </li><li class="sharify-btn-email">
                                                    <a title="Share via mail" href="mailto:?subject={{$result->post_title}}&body={{$options_email_msg}} - {{url()->current()}}">
                                                        <span class="sharify-icon"><i class="sharify sharify-mail"></i></span>
                                                        <span class="sharify-title">Email</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <a href="" id="whatsapp-share-btt" rel="nofollow" target="_blank"><img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/social-media.png" alt=""></a>
                                    </div>
                                </div>
                                <div class="interesse">
                                    <h2>Tenho interesse no imóvel</h2>
                                    @if(!empty($taxas_imovel))
                                        @foreach($taxas_imovel as $taxa_imovel)
                                        <div class="item-interesse">
                                            <p>{{$taxa_imovel['title']}}</p>
                                            <div class="linha"></div>
                                            <p class="valor">R$: {{$taxa_imovel['value']}}</p>
                                        </div>
                                        @endforeach
                                    @endif
                                    <div class="item-interesse-total">
                                        <p>Valor Total</p>
                                        <div class="linha"></div>
                                        <p class="valor">{{$result->__get('valor-total')}}</p>
                                    </div>
                                    <div class="info-interesse">
                                    <p>A ImóvelLuxo tem prazer em lhe atender e oferecer os melhores produtos do mercado imobiliário. No formulário a baixo você nos fornece seus dados para um contato inicial.</p>
                                    </div>
                                    <div class="form-interesse">
                                        <!-- Form de interesse -->
                                        <!-- <?php //echo do_shortcode( '[contact-form-7 id="92" title="Form-Empreendimento"]' ); ?> -->
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="mapa">
                            <h2>Localização</h2>
                            <!-- Aqui virá o mapa dinâmico -->
                            {!!$result->__get('mapa-imovel')!!}
                        </section>
                        @if(!empty($result->__get('video')))
                        <section class="video-imovel">
                            <!-- <video width="100%" autoplay loop muted>
                                <source src="" type="video/mp4">
                                <source src="movie.ogg" type="video/ogg">
                                Seu navegador não suporta este vídeo
                            </video> -->
                            {!! $video !!}
                        </section>
                        @endif
                        <section class="interesse-button">
                            <a href="#interesse">Tenho interesse no imóvel</a>
                        </section>
                    </article>
                </div>
                <!-- Chamada do template empreendimentos -->
                @include('partials.destaques')
                <!-- Modal fotos -->
                <!-- <?php //if(have_rows('galeria')): while(have_rows('galeria')) : the_row(); ?> -->
                    <section class="modal-fotos">
                        <!-- <?php //if(have_rows('imgs')): while(have_rows('imgs')) : the_row(); ?> -->
                            <div class="modal fade" id="id-img" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                                <div class="carousel-inner">
                                                  <div class="carousel-item active">
                                                    <img src="{{asset('content/themes/imovelluxo/assets/imgs/img-destaque2.jpg')}}" data-toggle="modal" data-target="#id-img">
                                                  </div>
                                                  <div class="carousel-item">
                                                    <img src="{{asset('content/themes/imovelluxo/assets/imgs/img-destaque2.jpg')}}" data-toggle="modal" data-target="#id-img">
                                                  </div>
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                  <span class="sr-only">Anterior</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                  <span class="sr-only">Próximo</span>
                                                </a>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- <?php //endwhile; else : endif; ?> -->
                    </section>
            </main>
            <script>
                $('.fotos-destaque').owlCarousel({
                    loop:true,
                    autoplay:true,
                    margin:10,
                    responsiveClass:true,
                    dots:false,
                    responsive:{
                        0:{
                            items:1,
                            nav:true
                        },
                        600:{
                            items:1,
                            nav:false
                        },
                        1000:{
                            items:1,
                            nav:true,
                        }
                    }
                })
            </script>
@endsection
