@extends('layouts.app')

@section('content')

    <main class="empreendimento">
        <section class="titulo-interno">
            <div class="container">
                <h1>Todos os Imóveis</h1>
            </div>
        </section>
        <div class="container">
            @include('partials.breadcrumbs')
        </div>
        <section class="content-empreendimento container">
            <div class="filter">

                <form action="{{route('search')}}" method="get" class="searchandfilter">
                    <div>
                        <ul>
                            <li>
                                <select  name='contrato' id='ofcontrato' class='postform' >
                                    <option value='0' selected='selected'>Todos Contratos</option>
                                    @foreach($contratos as $key => $contrato)
                                        <option class="level-0" value="{{$key}}">{{$contrato}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <select  name='tipo' id='oftipo' class='postform' >
                                    <option value='0' selected='selected'>Todos os tipos de empreendimentos</option>
                                    @foreach($tipos as $key => $tipo)
                                        <option class="level-0" value="{{$key}}">{{$tipo}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <select  name='bairro' id='ofbairro' class='postform' >
                                    <option value='0' selected='selected'>Todos os bairros</option>
                                    @foreach($bairros as $key => $bairro)
                                        <option class="level-0" value="{{$key}}">{{$bairro}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <select  name='tamanho' id='oftamanho' class='postform' >
                                    <option value='0' selected='selected'>Todos Tamanho</option>
                                    @foreach($tamanhos as $key => $tamanho)
                                        <option class="level-0" value="{{$key}}">{{$tamanho}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <select  name='quartos' id='ofquartos' class='postform' >
                                    <option value='0' selected='selected'>Todos Quartos</option>
                                    @foreach($quartos as $key => $quarto)
                                        <option class="level-0" value="{{$key}}">{{$quarto}}</option>
                                    @endforeach
                                </select>
                            </li>
                            <li>
                                <select  name='faixadepreco' id='offaixadepreco' class='postform' >
                                    <option value='0' selected='selected'>Todos Faixa de preço</option>
                                    @foreach($suites as $key => $suite)
                                        <option class="level-0" value="{{$key}}">{{$suite}}</option>
                                    @endforeach
                                </select>
                                <input type="submit" value="Buscar">
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
            <div class="row">
                @foreach($result as $rs)
                    @include('partials.empreendimento')
                @endforeach
            </div>
        </section>
    </main>
        @endsection
