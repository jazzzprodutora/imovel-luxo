@extends('layouts.app')

@section('content')
    <main class="home">
        <section class="hero">
            <div class="container">
                <h2>Bem vindo à ImóvelLuxo</h2>
                <h1>Nós temos  a solução em imóveis de alto padrão</h1>
                <div class="filter">

                    <form action="{{route('search')}}" method="get" class="searchandfilter">
                        <div>
                            <ul><li>
                                    <select  name='contrato' id='ofcontrato' class='postform' >
                                        <option value='0' selected='selected'> Você precisa</option>
                                        @foreach($contratos as $key => $contrato)
                                            <option class="level-0" value="{{$key}}">{{$contrato}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <select  name='tipo' id='oftipo' class='postform' >
                                        <option value='0' selected='selected'> Tipo de Imóvel </option>
                                        @foreach($tipos as $key => $tipo)
                                            <option class="level-0" value="{{$key}}">{{$tipo}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <select  name='bairro' id='ofbairro' class='postform' >
                                        <option value='0' selected='selected'>Bairro</option>
                                        @foreach($bairros as $key => $bairro)
                                            <option class="level-0" value="{{$key}}">{{$bairro}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <select  name='tamanho' id='oftamanho' class='postform' >
                                        <option value='0' selected='selected'> Área do imóvel</option>
                                        @foreach($tamanhos as $key => $tamanho)
                                            <option class="level-0" value="{{$key}}">{{$tamanho}}</option>
                                        @endforeach
                                    </select>
                                <li>
                                    <select  name='quartos' id='ofquartos' class='postform' >
                                        <option value='0' selected='selected'> Quartos</option>
                                        @foreach($quartos as $key => $quarto)
                                            <option class="level-0" value="{{$key}}">{{$quarto}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <select  name='faixadepreco' id='offaixadepreco' class='postform' >
                                        <option value='0' selected='selected'>Faixa de Preço</option>
                                        @foreach($faixadeprecos as $key => $faixadepreco)
                                            <option class="level-0" value="{{$key}}">{{$faixadepreco}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <select  name='suite' id='ofsuite' class='postform' >
                                        <option value='0' selected='selected'> Suítes</option>
                                        @foreach($suites as $key => $suite)
                                            <option class="level-0" value="{{$key}}">{{$suite}}</option>
                                        @endforeach
                                    </select>
                                </li>
                                <li>
                                    <input type="submit" value="Buscar">
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="links">
                    <h4>Links rápidos dos nossos destaques</h4>
                    <ul class='list-unstyled'>
                        <li><p class='name'><a href="{{route('search')}}?tipo=apartamento">Apartamento</a></p></li>
                        <li><p class='name'><a href="{{route('search')}}?tipo=casa">Casa</a></p></li>
                        <li><p class='name'><a href="{{route('search')}}?tipo=kitnet-flat-studio">Kitnet / Flat / Studio</a></p></li>
                    </ul>

                </div>
                <aside class="social-banner">
                    <ul class="list-unstyled">
                        <a href="https://www.instagram.com/imovelluxope/" target="_blank"><li><img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/instagram.svg" alt=""></li></a>
                        <a href="https://www.linkedin.com/company/imovelluxo" target="_blank"><li><img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/linkedin.svg" alt=""></li></a>
                        <a href="https://www.youtube.com/channel/UC7g9u0TtmTxzqR5tIibhG5A?view_as=subscriber" target="_blank"><li><img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/youtube.svg" alt=""></li></a>
                        <a href="https://www.facebook.com/imovelluxope" target="_blank"><li><img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/facebook.svg" alt=""></li></a>
                    </ul>
                </aside>
            </div>
        </section>
        <!--Imovel destaque-->

        <!-- Variavel pra pagar informações de um ACF de outra pagina-->
        <!-- Essa página é um template que se repete na home, e empreendimento destaque-->
        @include('partials.destaques')
    </main>
@endsection

