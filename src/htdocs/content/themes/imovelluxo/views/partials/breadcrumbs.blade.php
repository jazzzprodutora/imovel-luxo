<div class="breadcrumbs">
    <!-- Breadcrumb NavXT 6.4.0 -->
    @foreach($breadcrumbs as $breadcrumb)
        @if(empty($breadcrumb['route']))
            <span class="post post-page current-item">{{$breadcrumb['title']}}</span>
        @else
            <span property="itemListElement" typeof="ListItem">
                <a property="item" typeof="WebPage" title="{{$breadcrumb['title']}}" href="{{$breadcrumb['route']}}" class="home" >
                    <span property="name">{{$breadcrumb['title']}}</span>
                </a>
                {{--<meta property="position" content="1">--}}
            @if($breadcrumb != end($breadcrumb))
                </span> /
            @endif
        @endif
    @endforeach

</div>
