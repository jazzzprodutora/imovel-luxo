<section class="imovel-destaque">
    <h2>Imóveis em Destaque</h2>
    <div class="container">
        <div class="owl-carousel owl-theme imovel-destaque-cont">
            @foreach($posts_destaque as $post_destaque)
                @php
                    $thumbnail_medium = $post_destaque->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_MEDIUM);
                    $thumbnail_large = $post_destaque->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_LARGE);
                    $thumbnail_full = $post_destaque->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_FULL);
                    if(is_array($thumbnail_full)){
                        $thumbnail_full = $thumbnail_full['url'];
                    }
                    if(is_array($thumbnail_large)){
                        $thumbnail_large = $thumbnail_large['url'];
                    }
                @endphp
                <div class="item">
                    <div class="cont-item">
                        <a href="{{route('empreendimento', $post_destaque->post_name)}}">
                            <div class="img-destaque">
                                <picture class="img-imovel-item">
                                    <img width="{{$thumbnail_medium['width']}}" height="{{$thumbnail_medium['height']}}" src="{{$thumbnail_full}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="{{$thumbnail_large}} 487w, {{$thumbnail_medium['url']}} 212w" sizes="(max-width: 487px) 100vw, 487px" />
                                </picture>
                                <div class="icone">
                                    <img class="icone-saiba" src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/icons/icone-soma.svg">
                                    <span class="veja">SAIBA MAIS</span>
                                </div>
                                <div class="tipo-empreendimento" id="Venda">
                                    <span>{{$post_destaque->__get('tipo_de_contrato')}}</span>
                                </div>
                            </div>
                            <div class="legenda-imovel">
                                <h3>{{$post_destaque->post_title}}</h3>
                                <span class="local-empreendimento">{{$post_destaque->__get('endereco-imovel')}}</span>
                                <span class="local-empreendimento">Preço: R$ {{$post_destaque->__get('valor-total')}}</span>
                                <span class="local-empreendimento">Tamanho:{{$post_destaque->__get('info_metros')}}</span>
                                <span class="local-empreendimento">{{$post_destaque->__get('info_quartos')}}</span>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container todos-imv">
        <a href="empreendimentos/">Ver todos os imoveis em destaque</a>
    </div>
</section>

<script>
    $('.imovel-destaque-cont').owlCarousel({
        loop:true,
        autoplay:true,
        margin:10,
        responsiveClass:true,
        dots:false,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:false
            },
            1000:{
                items:3,
                nav:true,
            }
        }
    })
</script>
