@php
    $thumbnail_medium = $rs->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_MEDIUM);
    $thumbnail_large = $rs->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_LARGE);
    $thumbnail_full = $rs->thumbnail->size(\Corcel\Model\Meta\ThumbnailMeta::SIZE_FULL);
    if(is_array($thumbnail_full)){
        $thumbnail_full = $thumbnail_full['url'];
    }
    if(is_array($thumbnail_large)){
        $thumbnail_large = $thumbnail_large['url'];
    }
@endphp
<div class="col-sm-4">

    <a href="{{route('empreendimento', $rs->post_name)}}">
        <article class="item-empreendimento">
            <div class="img-destaque">
                <picture class="img-destaque-item">
                    <img width="{{$thumbnail_medium['width']}}" height="{{$thumbnail_medium['height']}}" src="{{$thumbnail_full}}" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="" srcset="{{$thumbnail_large}} 487w, {{$thumbnail_medium['url']}} 212w" sizes="(max-width: 487px) 100vw, 487px" />
                </picture>
                <div class="icone">
                    <img class="icon-contato" src="{{asset('content/themes/imovelluxo/assets/icons/icone-soma.svg')}}">
                    <span class="veja">SAIBA MAIS</span>
                </div>
            </div>
            <div class="titulos-empreendimento">
                <h1>{{$rs->post_title}}</h1>
                <span class="local-empreendimento">{{$rs->__get('endereco-imovel')}}</span>
                <span class="local-empreendimento">Preço: R$ {{$rs->__get('valor-total')}}</span>
                <span class="local-empreendimento">Tamanho: {{$rs->__get('info_metros')}}</span>
                <span class="local-empreendimento">{{$rs->__get('info_quartos')}}</span>
            </div>
        </article>
    </a>
</div>
