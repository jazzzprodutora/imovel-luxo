

<!-- header-->
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="{{asset('content/themes/imovelluxo/assets/icons/fav.png')}}">
    <!-- Bootstrap-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('content/themes/imovelluxo/assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('content/themes/imovelluxo/assets/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('content/themes/imovelluxo/assets/css/main.css')}}">
    <link rel='stylesheet' id='sharify-font-css'  href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400' type='text/css' media='all' />
    <link rel="stylesheet" href="{{asset('content/themes/imovelluxo/assets/css/style.css')}}">
    <script src="{{asset('content/themes/imovelluxo/assets/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('content/themes/imovelluxo/assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('content/themes/imovelluxo/assets/js/bootstrap.bundle.js')}}"></script>
    <title>Imovel luxo |</title>
</head>
<body >
<!-- Cabeçalho -->
<header>
    <div class="cont-header container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="conteudo-header">
                <a class="navbar-brand" href="#"><img src="{{asset('content/themes/imovelluxo/assets/icons/logo.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul id="menu-menu-principal" class="menu">
                    <li class="{{(Route::current()->getName() == 'home')?'current_page_item':''}}"><a href="{{route('home')}}">Home</a></li>
                    <li class="{{(Route::current()->getName() == 'empreendimentos')?'current_page_item':''}}"><a href="{{route('empreendimentos')}}">Imóveis</a></li>
                    <li class="{{(Route::current()->getName() == 'venda')?'current_page_item':''}}"><a href="{{route('venda')}}">Venda seu Imóveil</a></li>
                    <li class="{{(Route::current()->getName() == 'quem-somos')?'current_page_item':''}}"><a href="{{route('quem-somos')}}">Quem Somos</a></li>
                    <li class="{{(Route::current()->getName() == 'noticias')?'current_page_item':''}}"><a href="{{route('noticias')}}">Notícias Imobiliárias</a></li>
                    <li class="{{(Route::current()->getName() == 'contato')?'current_page_item':''}}"><a href="{{route('contato')}}">Contato</a></li>
                </ul>
            </div>
            <div class="social-header">
                <a href="tel:30403200"><img src="{{asset('content/themes/imovelluxo/assets/icons/call.svg')}}" alt="">(81) 3040-3200</a>
                <a href="https://api.whatsapp.com/send?phone=558192644424&" target="_blank"><img src="{{asset('content/themes/imovelluxo/assets/icons/whatsapp.png')}}" alt="whatsapp"></a>
            </div>
        </nav>
    </div>
</header>
@yield('content')
<footer>
    <div class="cont-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-5 mapa-site">
                    <h5>Mapa do Site</h5>
                    <ul class="list-unstyled">
                        <div class="">
                            <li><a href="{{route('home')}}">Home</a></li>
                            <li><a href="{{route('empreendimentos')}}">Empreendimentos</a></li>
                            <li><a href="{{route('venda')}}">Venda seu imóvel</a></li>
                            <li><a href="{{route('quem-somos')}}">Quem Somos</a></li>
                            <li><a href="{{route('contato')}}">Fale Conosco</a></li>
                        </div>
                        <div class="">
                            <li><a href="{{route('politicas')}}/#privacidade">Política de Privacidade</a></li>
                            <li><a href="{{route('politicas')}}/#qualidade">Política de Qualidade</a></li>
                            <li><a href="{{route('politicas')}}/#seguranca">Dicas de Segurança</a></li>
                            <li><a href="{{route('politicas')}}/#termos">Termos de uso</a></li>
                        </div>
                    </ul>
                </div>
                <div class="col-md-3 contatos">
                    <h5>Contatos</h5>
                    <address>
                        <a href="">
                            <ul class="list-unstyled">
                                <li>Rua Antônio Lumack, 128, Boa Viagem</li>
                                <li>Recife - PE</li>
                                <li>CEP 51020-350</li>
                            </ul>
                        </a>
                    </address>
                    <div class="tel">
                        <a href="tel:30403200">(81) 3040-3200</a>
                        <a  href="https://api.whatsapp.com/send?phone=558192644424&" target="_blank">(81) 9264-4424</a>
                    </div>
                </div>
                <div class="col-md-4 news">
                    <h5>Newsletter</h5>
                    <span>Assine nossa newsletter</span>
                    <?php echo do_shortcode( '[contact-form-7 id="157" title="News"]' ); ?>
                    <br />
                    <div class="social-footer">
                        <ul class="list-unstyled">
                            <li class="item1"><span>Redes</span><span>Sociais</span> </li>
                            <li><a href="https://www.instagram.com/imovelluxope" target="_blank"><img src="{{asset('content/themes/imovelluxo/assets/icons/insta-footer.jpg')}}"  alt=""></a></li>
                            <li><a href="https://www.facebook.com/imovelluxope" target="_blank"><img src="{{asset('content/themes/imovelluxo/assets/icons/face-footer.jpg')}}"  alt=""></a></li>
                            <li><a href="https://www.youtube.com/channel/UC7g9u0TtmTxzqR5tIibhG5A?view_as=subscriber" target="_blank"><img src="{{asset('content/themes/imovelluxo/assets/icons/youtube-footer.jpg')}}"  alt=""></a></li>
                            <li><a href="https://www.linkedin.com/company/imovelluxo" target="_blank"><img src="{{asset('content/themes/imovelluxo/assets/icons/linkdin-footer.jpg')}}"  alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="jazzz">
        <div class="container cont-jazzz">
            <span>2020 Ⓒ Imovelluxo - CNPJ: 05.119.098.0001/34 </span>
            <a href="http://jazzz.com.br/" target="_blank"><img src="{{asset('content/themes/imovelluxo/assets/icons/jazzz.svg')}}" title="Desenvolvido pela Jazzz Agência Digital" alt="Jazzz Agência Digital"></a>
        </div>
    </div>
</footer>
<script src="{{asset('content/themes/imovelluxo/assets/js/main.js')}}"></script>
<!-- Essse script está aqui pq as vezes bug quando estar em um js externo, script do carrosel -->
<script>
    $('.fotos-destaque').owlCarousel({
        loop:true,
        autoplay:true,
        margin:10,
        responsiveClass:true,
        dots:false,
        responsive:{
            0:{
                items:1,
                nav:true
            },
            600:{
                items:1,
                nav:false
            },
            1000:{
                items:1,
                nav:true,
            }
        }
    })
</script>
</body>
</html>
