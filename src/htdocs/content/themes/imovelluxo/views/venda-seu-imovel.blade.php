@extends('layouts.app')

@section('content')
    <main class="venda-imovel">
        <section class="titulo-interno">
            <div class="container">
                <h1>Vendemos seu Imovel</h1>
            </div>
        </section>
        <div class="container">
            @include('partials.breadcrumbs')
        </div>
        <section class="venda-imovel-cont container">
            <div class="info">
                <h2>Proprietário, avaliamos, vendemos e alugamos seu imóvel em tempo recorde.
                    <br>Temos clientes cadastrados à espera do seu empreendimento.</h2>
                <div class="quero-indicar">
                    <a href="#indicar">Quero Cadastrar meu imóvel</a>
                </div>
            </div>
            <picture class="ilustra-venda">
                <source media="(max-width:460px)" srcset="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/imgs/ilustra-sm.svg">
                <img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/imgs/ilustra.svg" alt="">
            </picture>
        </section>
        <section class="cadastre" id="indicar">
            <div class="container conteudo-cadastro">
                <div class="passos">
                    <h2>Cadastro de imóvel</h2>
                    <picture class="passos-cadastro">
                        <source media="(max-width:460px)" srcset="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/imgs/passos-sm.svg">
                        <img src="http://imovelluxo.com.br/wp-content/themes/imovelluxo/assets/imgs/passos.svg" alt="">
                    </picture>
                </div>
                <div class="form-venda">
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner">
                            <?php echo do_shortcode( '[contact-form-7 id="49" title="Venda de Imóveis"]' ); ?>
                         </div>
                        <!-- <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a> -->
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
