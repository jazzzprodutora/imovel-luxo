<?php

namespace App\Http\Controllers;

use Cohensive\Embed\Facades\Embed;
use Corcel\Acf\AdvancedCustomFields;
use Corcel\Model\Meta\PostMeta;
use Corcel\Model\Option;
use Corcel\Model\Post;
use Corcel\Model\Taxonomy;
use Corcel\Model\Term;
use Illuminate\Http\Request;


class PageController extends Controller
{
    public function home()
    {
        $contratos = array();
        $contratos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'contrato')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($contratos_tax as $contrato){
            if($contrato->posts()->type('empreendimento')->status('publish')->first() != null) {
                $contratos[$contrato->term->slug] = $contrato->term->name;
            }
        }

        $tipos = array();
        $tipos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'tipo')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($tipos_tax as $tipo){
            if($tipo->posts()->type('empreendimento')->status('publish')->first() != null) {
                $tipos[$tipo->term->slug] = $tipo->term->name;
            }
        }

        $bairros = array();
        $bairros_tax = Taxonomy::with('term')
            ->where('taxonomy', 'bairro')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($bairros_tax as $bairro){
            if($bairro->posts()->type('empreendimento')->status('publish')->first() != null) {
                $bairros[$bairro->term->slug] = $bairro->term->name;
            }
        }

        $tamanhos = array();
        $tamanhos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'tamanho')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($tamanhos_tax as $tamanho){
            if($tamanho->posts()->type('empreendimento')->status('publish')->first() != null) {
                $tamanhos[$tamanho->term->slug] = $tamanho->term->name;
            }
        }

        $quartos = array();
        $quartos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'quartos')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($quartos_tax as $quarto){
            if($quarto->posts()->type('empreendimento')->status('publish')->first() != null) {
                $quartos[$quarto->term->slug] = $quarto->term->name;
            }
        }

        $faixadeprecos = array();
        $faixadeprecos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'faixadepreco')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($faixadeprecos_tax as $faixadepreco){
            if($faixadepreco->posts()->type('empreendimento')->status('publish')->first() != null) {
                $faixadeprecos[$faixadepreco->term->slug] = $faixadepreco->term->name;
            }
        }

        $suites = array();
        $suites_tax = Taxonomy::with('term')
            ->where('taxonomy', 'suite')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($suites_tax as $suite){
            if($suite->posts()->type('empreendimento')->status('publish')->first() != null) {
                $suites[$suite->term->slug] = $suite->term->name;
            }
        }
        $destaques = Post::type('empreendimento')->status('publish')->orderBy('post_date', 'DESC')->get();
        foreach ($destaques as $tax_destaque){
            if($tax_destaque->thumbnail()){
                $posts_destaque[] = $tax_destaque;
            }
        }

        return view( 'home', array('posts_destaque' => $posts_destaque, 'contratos' => $contratos, 'tipos' => $tipos, 'bairros' => $bairros, 'tamanhos' => $tamanhos, 'quartos' => $quartos, 'faixadeprecos' => $faixadeprecos, 'suites' => $suites));
    }
    public function search(Request $request)
    {
        $q = Post::type('empreendimento')->status('publish')->orderBy('post_date', 'DESC');
        if($request->input('contrato') == '0'
            && $request->input('tipo') == '0'
            && $request->input('bairro') == '0'
            && $request->input('tamanho') == '0'
            && $request->input('quartos') == '0'
            && $request->input('faixadepreco') == '0'
            && $request->input('suite') == '0'
        ){
            $result = $q->get();
        }else{
            $result = array();

            $posts = $q->get();
            $pass = false;
            foreach ($posts as $key => $post) {
                foreach ($request->all() as $search_field => $search_term) {
                    if ($search_term != '0') {
                        if ($post->hasTerm($search_field, $search_term)) {
                            $pass = true;
                        } else {
                            $pass = false;
                            break;
                        }
                    }
                }
                if($pass == true){
                    $result[$key] = $post;
                }
            }
        }

        if(empty($request->all()))
        {
            $result = $q->get();
        }

        return view('search', ['result' => $result]);
    }
    public function politicas()
    {
        return view('politicas');
    }
    public function contato()
    {
        $breadcrumbs = [
            [
                'title' => 'Imóvel Luxo',
                'route' => route('home')
            ],
            [
                'title' => 'Contato'
            ]
        ];
        return view('contato', array('breadcrumbs' => $breadcrumbs));
    }

    public function empreendimentos()
    {
        $contratos = array();
        $contratos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'contrato')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($contratos_tax as $contrato){
            if($contrato->posts()->type('empreendimento')->status('publish')->first() != null) {
                $contratos[$contrato->term->slug] = $contrato->term->name;
            }
        }

        $tipos = array();
        $tipos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'tipo')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($tipos_tax as $tipo){
            if($tipo->posts()->type('empreendimento')->status('publish')->first() != null) {
                $tipos[$tipo->term->slug] = $tipo->term->name;
            }
        }

        $bairros = array();
        $bairros_tax = Taxonomy::with('term')
            ->where('taxonomy', 'bairro')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($bairros_tax as $bairro){
            if($bairro->posts()->type('empreendimento')->status('publish')->first() != null) {
                $bairros[$bairro->term->slug] = $bairro->term->name;
            }
        }

        $tamanhos = array();
        $tamanhos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'tamanho')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($tamanhos_tax as $tamanho){
            if($tamanho->posts()->type('empreendimento')->status('publish')->first() != null) {
                $tamanhos[$tamanho->term->slug] = $tamanho->term->name;
            }
        }

        $quartos = array();
        $quartos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'quartos')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($quartos_tax as $quarto){
            if($quarto->posts()->type('empreendimento')->status('publish')->first() != null) {
                $quartos[$quarto->term->slug] = $quarto->term->name;
            }
        }

        $faixadeprecos = array();
        $faixadeprecos_tax = Taxonomy::with('term')
            ->where('taxonomy', 'faixadepreco')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($faixadeprecos_tax as $faixadepreco){
            if($faixadepreco->posts()->type('empreendimento')->status('publish')->first() != null) {
                $faixadeprecos[$faixadepreco->term->slug] = $faixadepreco->term->name;
            }
        }

        $suites = array();
        $suites_tax = Taxonomy::with('term')
            ->where('taxonomy', 'suite')
            ->get()
            ->sortBy(function($term) {
                return $term->term->name;
            });
        foreach($suites_tax as $suite){
            if($suite->posts()->type('empreendimento')->status('publish')->first() != null) {
                $suites[$suite->term->slug] = $suite->term->name;
            }
        }
        $result = Post::type('empreendimento')->status('publish')->orderBy('post_date', 'DESC')->get();;

        $breadcrumbs = [
            [
                'title' => 'Imóvel Luxo',
                'route' => route('home')
            ],
            [
                'title' => 'Empreendimentos'
            ]
        ];
        return view( 'empreendimentos', array('breadcrumbs' => $breadcrumbs, 'result' => $result, 'contratos' => $contratos, 'tipos' => $tipos, 'bairros' => $bairros, 'tamanhos' => $tamanhos, 'quartos' => $quartos, 'faixadeprecos' => $faixadeprecos, 'suites' => $suites));
    }

    public function empreendimento($slug)
    {
        $result = Post::type('empreendimento')->status('publish')->where('post_name', $slug)->orderBy('post_date', 'DESC')->first();;
        $post_meta = PostMeta::where('meta_key','LIKE', 'galeria_%')->where('post_id', $result->ID)->get();
        $post_gallery = array();
        foreach($post_meta as $meta){
            if(!empty($meta->meta_value)) {
                $post_images = PostMeta::where('meta_key','LIKE', '_wp_attached_file')->where('post_id', $meta->meta_value)->get();
                foreach ($post_images as $post_image){
                    $post_gallery[] = $post_image->meta_value;
                }
            }
        }
        $post_meta_detalhes = PostMeta::where('meta_key','LIKE', 'detalhes-imovel_%')->where('post_id', $result->ID)->get();
        $detalhes = array();
        foreach($post_meta_detalhes as $post_meta_detalhe){
            $detalhes[] = $post_meta_detalhe->meta_value;
        }
        $detalhes_imovel = array();
        foreach (array_chunk($detalhes, 2) as $key => $detalhe){
            $detalhe_icon = PostMeta::where('meta_key','LIKE', '_wp_attached_file')->where('post_id', $detalhe[0])->first();
            $detalhes_imovel[$key]['icon'] = $detalhe_icon->meta_value;
            $detalhes_imovel[$key]['info'] = $detalhe[1];
        }

        $post_meta_taxas = PostMeta::where('meta_key','LIKE', 'taxas_%')->where('post_id', $result->ID)->get();
        $taxas = array();
        foreach($post_meta_taxas as $post_meta_taxa){
            $taxas[] = $post_meta_taxa->meta_value;
        }
        $taxas_imovel = array();
        foreach (array_chunk($taxas, 2) as $key => $taxa){
            $taxas_imovel[$key]['title'] = $taxa[0];
            $taxas_imovel[$key]['value'] = $taxa[1];
        }

        $video = null;
        $embed = Embed::make($result->__get('video'))->parseUrl();
        if ($embed) {
            $embed->setAttribute(['width' => 200]);
            $video = $embed->getHtml();
        }
        $destaques = Post::type('empreendimento')->status('publish')->orderBy('post_date', 'DESC')->get();
        foreach ($destaques as $tax_destaque){
            if($tax_destaque->thumbnail()){
                $posts_destaque[] = $tax_destaque;
            }
        }
        $post_gallery = array_chunk($post_gallery, 4);
        $options = Option::where('option_name', 'sharify_custom_email_msg')->first();
        $options_email_msg = $options->option_value;
        return view('empreendimento', ['options_email_msg' => $options_email_msg,'taxas_imovel' => $taxas_imovel, 'detalhes_imovel' => $detalhes_imovel, 'posts_destaque' => $posts_destaque, 'video' => $video, 'result'=> $result, 'post_gallery' => $post_gallery]);
    }

    public function noticias()
    {
        $result = Post::type('noticia')->status('publish')->orderBy('post_date', 'DESC')->get();

        $breadcrumbs = [
            [
                'title' => 'Imóvel Luxo',
                'route' => route('home')
            ],
            [
                'title' => 'Notícias Imobiliárias'
            ]
        ];
        return view('noticias', array('result' => $result, 'breadcrumbs' => $breadcrumbs));
    }

    public function noticia($slug)
    {
        $result = Post::type('noticia')->status('publish')->where('post_name', $slug)->orderBy('post_date', 'DESC')->first();
        $video = null;
        $embed = Embed::make($result->__get('videos_0_video_noticia'))->parseUrl();
        if ($embed) {
            $embed->setAttribute(['width' => 650]);
            $video = $embed->getHtml();
        }
        $noticias = Post::type('noticia')->status('publish')->orderBy('post_date', 'DESC')->get();
        $options = Option::where('option_name', 'sharify_custom_email_msg')->first();
        $options_email_msg = $options->option_value;
        $breadcrumbs = [
            [
                'title' => 'Imóvel Luxo',
                'route' => route('home')
            ],
            [
                'title' => 'Notícias Imobiliárias'
            ]
        ];

        return view('noticia', array('options_email_msg' => $options_email_msg, 'video' => $video, 'noticias' => $noticias, 'result' => $result, 'breadcrumbs' => $breadcrumbs));
    }

    public function quemSomos()
    {
        $array_equipes = array();
        $equipes = PostMeta::where('meta_key','LIKE', 'item-equipe_%')->get();
        foreach ($equipes as $key => $equipe){
            $post_equipe = Post::status('publish')->where('ID', $equipe->post_id)->first();
            if(!empty($post_equipe)) {
                $field = substr($equipe->meta_key, 23);
                if(!empty($field)) {
                    if ($field == 'imagem') {
                        $equipe_image = PostMeta::where('meta_key', 'LIKE', '_wp_attached_file')->where('post_id', $equipe->meta_value)->first();
                        $array_equipes[$equipe->post_id][$field] = $equipe_image->meta_value;
                    }else{
                        $array_equipes[$equipe->post_id][$field] = $equipe->meta_value;
                    }
                }
            }
        }
        $breadcrumbs = [
            [
                'title' => 'Imóvel Luxo',
                'route' => route('home')
            ],
            [
                'title' => 'Quem Somos'
            ]
        ];
        return view('quem-somos', array('array_equipes' => $array_equipes,'breadcrumbs' => $breadcrumbs));
    }

    public function venda()
    {
        $breadcrumbs = [
            [
                'title' => 'Imóvel Luxo',
                'route' => route('home')
            ],
            [
                'title' => 'Venda seu Imóvel'
            ]
        ];
        return view('venda-seu-imovel', array('breadcrumbs' => $breadcrumbs));
    }




}
